package com.wavelabs.resource.test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.model.ZipCode;
import com.wavelabs.resource.ZipCodeResource;
import com.wavelabs.service.ZipCodeService;
import com.wavelabs.service.test.DataBuilder;

@RunWith(MockitoJUnitRunner.class)
public class ZipCodeResourceTest {
	@Mock
	ZipCodeService zipCodeService;

	@InjectMocks
	ZipCodeResource zipCodeResource;

	@Test
	public void testAddZipCode() {
		ZipCode zipCode = DataBuilder.getZipCode();
		when(zipCodeService.addZipCode(any(ZipCode.class))).thenReturn(zipCode);
		ResponseEntity<ZipCode> entity = zipCodeResource.addZipCode(zipCode);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

}
