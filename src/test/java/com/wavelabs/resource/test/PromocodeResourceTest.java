package com.wavelabs.resource.test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.model.PromoCode;
import com.wavelabs.resource.PromocodeResource;
import com.wavelabs.service.PromocodeService;
import com.wavelabs.service.test.DataBuilder;

@RunWith(MockitoJUnitRunner.class)
public class PromocodeResourceTest {
	
	@Mock
	PromocodeService promoCodeService;
	
	@InjectMocks
	PromocodeResource promocodeResource;
	
	@Test
	public void testAddPromoCode(){
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(promoCodeService.addPromoCode(any(PromoCode.class))).thenReturn(promoCode);
		ResponseEntity<PromoCode> entity = promocodeResource.addPromoCode(promoCode);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
}
