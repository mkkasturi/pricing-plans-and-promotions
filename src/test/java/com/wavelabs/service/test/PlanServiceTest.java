package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

import com.wavelabs.dao.PlanDao;
import com.wavelabs.model.Plan;
import com.wavelabs.service.PlanService;

@RunWith(MockitoJUnitRunner.class)
public class PlanServiceTest {

	@Mock
	PlanDao planDao;
	
	@Mock
	PlanService planServiceMock;

	@InjectMocks
	PlanService planService;
	
	@Mock
	Session session;
	
	@Mock
	Query query;

	@Test
	public void testAddPlan() {
		Plan plan = DataBuilder.getPlan();
		when(planDao.persistPlan(any(Plan.class), anyInt())).thenReturn(plan);
		Plan plan1 = planService.addPlan(plan, 1);
		Assert.assertEquals(plan, plan1);
	}
	
	@Test
	public void testGetPlan(){
		Plan plan = DataBuilder.getPlan();
		when(planDao.getPlan(anyInt())).thenReturn(plan);
		Plan plan1 = planService.getPlan(1);
		Assert.assertEquals(plan.getId(), plan1.getId());
	}
	
	@Test
	public void testDeletePlan(){
		PowerMockito.doNothing().when(planDao).deletePlan(anyInt());
		planService.deletePlan(1);
	}
	
	@Test
	public void testIsPlanIf(){
		Plan plan=DataBuilder.getPlan();
		when(planServiceMock.getPlan(anyInt())).thenReturn(plan);
		boolean result=planService.isPlan(1);
		Assert.assertEquals(true, result);
	}
	
	@Test
	public void testIsPlanElse(){
		Plan plan=DataBuilder.getFreeTrial();
		when(planServiceMock.getPlan(anyInt())).thenReturn(plan);
		boolean result=planService.isPlan(0);
		Assert.assertEquals(false, result);
	}
}
