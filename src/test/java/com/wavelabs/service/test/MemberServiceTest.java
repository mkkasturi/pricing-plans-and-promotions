package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.dao.MemberDao;
import com.wavelabs.model.Member;
import com.wavelabs.service.MemberService;

@RunWith(MockitoJUnitRunner.class)
public class MemberServiceTest {

	@Mock
	MemberDao memberDao;

	@InjectMocks
	MemberService memberService;

	@Test
	public void testAddMember() {
		Member member = DataBuilder.getMember();
		when(memberDao.persistMember(any(Member.class))).thenReturn(member);
		Member member1 = memberService.addMember(member);
		Assert.assertEquals(member, member1);
	}

	@Test
	public void testGetMember() {
		Member member = DataBuilder.getMember();
		when(memberDao.getMember(anyInt())).thenReturn(member);
		Member member1 = memberService.getMember(1);
		Assert.assertEquals(member.getId(), member1.getId());
	}

	@Test
	public void testDeleteMember() {
		Member member = DataBuilder.getMember();
		when(memberDao.deleteMember(anyInt())).thenReturn(member);
		Member member1 = memberService.deleteMember(1);
		Assert.assertEquals(member.getId(), member1.getId());
	}
	
	@Test
	public void testUpdateMember(){
		Member member = DataBuilder.getMember();
		when(memberDao.updateMember(any(Member.class))).thenReturn(member);
		Member member1 = memberService.updateMember(member);
		Assert.assertEquals(member.getId(), member1.getId());
	}
}
