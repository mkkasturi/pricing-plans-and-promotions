package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.dao.ZipCodeDao;
import com.wavelabs.model.Scheme;
import com.wavelabs.model.ZipCode;
import com.wavelabs.service.SchemeService;
import com.wavelabs.service.ZipCodeService;

@RunWith(MockitoJUnitRunner.class)
public class ZipCodeServiceTest {

	@Mock
	ZipCodeDao zipCodeDao;

	@InjectMocks
	ZipCodeService zipCodeService;
	
	@InjectMocks
	SchemeService schemeService;

	@Test
	public void testAddZipCode() {
		ZipCode zipCode = DataBuilder.getZipCode();
		when(zipCodeDao.persistZipCode(zipCode)).thenReturn(zipCode);
		ZipCode zipCode1 = zipCodeService.addZipCode(zipCode);
		Assert.assertEquals(zipCode, zipCode1);
	}
	
	@Test
	public void testGetZipCode(){
		ZipCode zipCode = DataBuilder.getZipCode();
		when(zipCodeDao.getZipCode(anyInt())).thenReturn(zipCode);
		ZipCode zipCode1 = zipCodeService.getZipCode(1);
		Assert.assertEquals(zipCode.getId(), zipCode1.getId());
	}
	
}
