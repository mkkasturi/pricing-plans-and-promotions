package com.wavelabs.dao.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.model.Scheme;
import com.wavelabs.service.test.DataBuilder;
import com.wavelabs.utility.Helper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Helper.class)
public class SchemeDaoTest {
	private Session session;

	@Mock
	Transaction transaction;

	@Before
	public void setUp() {
		session = mock(Session.class);
		PowerMockito.mockStatic(Helper.class);
		when(Helper.getSession()).thenReturn(session);
	}
	@Test
	public void testGetScheme() {
		Scheme scheme = DataBuilder.getScheme();
		when(session.get(eq(Scheme.class), any())).thenReturn(scheme);
		Scheme scheme1 = ObjectBuilder.getSchemeDao().getScheme(1);
		Assert.assertEquals(scheme, scheme1);
	}
	
	@Test
	public void testPersistScheme(){
		Scheme scheme = DataBuilder.getScheme();
		when(session.save(any(Scheme.class))).thenReturn(123);
		when(session.beginTransaction()).thenReturn(transaction);
		Scheme scheme1 = ObjectBuilder.getSchemeDao().persistScheme(scheme);
		Assert.assertEquals(scheme, scheme1);
	}	
}
