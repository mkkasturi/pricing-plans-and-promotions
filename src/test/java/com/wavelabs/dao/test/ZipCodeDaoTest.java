package com.wavelabs.dao.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.model.ZipCode;
import com.wavelabs.service.test.DataBuilder;
import com.wavelabs.utility.Helper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Helper.class)
public class ZipCodeDaoTest {
	private Session session;

	@Mock
	Transaction transaction;

	@Before
	public void setUp() {
		session = mock(Session.class);
		PowerMockito.mockStatic(Helper.class);
		when(Helper.getSession()).thenReturn(session);
	}
	
	@Test
	public void testGetZipCode() {
		ZipCode zipCode = DataBuilder.getZipCode();
		when(session.get(eq(ZipCode.class), any())).thenReturn(zipCode);
		ZipCode zipCode1 = ObjectBuilder.getZipCodeDao().getZipCode(1);
		Assert.assertEquals(zipCode,zipCode1);
	}
	
	@Test
	public void testPersistZipCode(){
		ZipCode zipCode = DataBuilder.getZipCode();
		when(session.save(any(ZipCode.class))).thenReturn(123);
		when(session.beginTransaction()).thenReturn(transaction);
		ZipCode zipCode1 = ObjectBuilder.getZipCodeDao().persistZipCode(zipCode);
		Assert.assertEquals(zipCode, zipCode1);
	}
	
	@Test
	public void testUpdateZipCode(){
		ZipCode zipCode = DataBuilder.getZipCode();
		PowerMockito.doNothing().when(session).saveOrUpdate(any(ZipCode.class));
		when(session.beginTransaction()).thenReturn(transaction);
		ZipCode zipCode1 = ObjectBuilder.getZipCodeDao().updateZipCode(zipCode);
		Assert.assertEquals(zipCode, zipCode1);
	}

}
