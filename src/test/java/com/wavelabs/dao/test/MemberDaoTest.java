package com.wavelabs.dao.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.model.Member;
import com.wavelabs.service.test.DataBuilder;
import com.wavelabs.utility.Helper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Helper.class)
public class MemberDaoTest {

	private Session session;
	
	@Mock
	Transaction transaction;

	@Before
	public void setUp() {
		session = mock(Session.class);
		PowerMockito.mockStatic(Helper.class);
		when(Helper.getSession()).thenReturn(session);
	}

	@Test
	public void testGetMember() {
		Member member = DataBuilder.getMember();
		when(session.get(eq(Member.class), any())).thenReturn(member);
		Member member1 = ObjectBuilder.getMemberDao().getMember(1);
		Assert.assertEquals(member, member1);
	}
	
	@Test
	public void testPersistMember(){
		Member member = DataBuilder.getMember();
		when(session.save(any(Member.class))).thenReturn(123);
		when(session.beginTransaction()).thenReturn(transaction);
		Member member1 = ObjectBuilder.getMemberDao().persistMember(member);
		Assert.assertEquals(member, member1);
	}
	
	@Test
	public void testUpdateMember(){
		Member member = DataBuilder.getMember();
		PowerMockito.doNothing().when(session).saveOrUpdate(any(Member.class));
		when(session.beginTransaction()).thenReturn(transaction);
		Member member1 = ObjectBuilder.getMemberDao().updateMember(member);
		Assert.assertEquals(member, member1);
	}
	
	@Test
	public void testDeleteMember(){
		Member member = DataBuilder.getMember();
		when(session.get(eq(Member.class), any())).thenReturn(member);
		PowerMockito.doNothing().when(session).delete(any(Member.class));
		when(session.beginTransaction()).thenReturn(transaction);
		Member member1 = ObjectBuilder.getMemberDao().deleteMember(1);
		Assert.assertEquals(member, member1);
	}
}
