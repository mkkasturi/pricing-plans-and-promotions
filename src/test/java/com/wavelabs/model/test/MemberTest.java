package com.wavelabs.model.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.model.Member;
import com.wavelabs.model.Plan;
import com.wavelabs.model.PromoCode;

public class MemberTest {
	
	private static final double DELTA = 1e-15;

	@Test
	public void testGetId() {
		Member member = new Member();
		member.setId(12);
		Assert.assertEquals(12, member.getId());
	}

	@Test
	public void testGetEmail() {
		Member member = new Member();
		member.setEmail("mkkasturi@gmail.com");
		Assert.assertEquals("mkkasturi@gmail.com", member.getEmail());
	}

	@Test
	public void testGetPlan() {
		Member member = new Member();
		Plan plan = new Plan();
		member.setPlan(plan);
		Assert.assertEquals(plan, member.getPlan());
	}

	@Test
	public void testGetNextSubscriptionPlan() {
		Member member = new Member();
		Plan plan = new Plan();
		member.setNextSubscriptionPlan(plan);
		Assert.assertEquals(plan, member.getNextSubscriptionPlan());
	}

	@Test
	public void testGetPlanRenewalDate() {
		Member member = new Member();
		member.setPlanRenewalDate(new Date(1403685556000L));
		Assert.assertEquals(new Date(1403685556000L), member.getPlanRenewalDate());
	}

	@Test
	public void testGetFreeTrialExpiryDate() {
		Member member = new Member();
		member.setFreeTrialExpiryDate(new Date(1403685556000L));
		Assert.assertEquals(new Date(1403685556000L), member.getFreeTrialExpiryDate());
	}

	@Test
	public void testGetFreeTrialPromoCode() {
		Member member = new Member();
		PromoCode promoCode = new PromoCode();
		member.setFreeTrialPromoCode(promoCode);
		Assert.assertEquals(promoCode, member.getFreeTrialPromoCode());
	}

	@Test
	public void testGetUsedPromoCodes() {
		Member member = new Member();
		PromoCode promoCode1 = new PromoCode();
		PromoCode promoCode2 = new PromoCode();
		PromoCode promoCode3 = new PromoCode();
		Set<PromoCode> promoCodes = new HashSet<PromoCode>();
		promoCodes.add(promoCode1);
		promoCodes.add(promoCode2);
		promoCodes.add(promoCode3);
		member.setUsedPromoCodes(promoCodes);
		Assert.assertEquals(promoCodes, member.getUsedPromoCodes());

	}
	
	@Test
	public void testGetAmountPaidForPresentPlan(){
		Member member = new Member();
		member.setAmountPaidForPresentPlan(500.0);
		Assert.assertEquals(500.0, member.getAmountPaidForPresentPlan(), DELTA);
	}

}
