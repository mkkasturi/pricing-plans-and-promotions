package com.wavelabs.model;

import java.sql.Timestamp;
import java.util.Date;

public class MemberEvent {
	private int id;
	private int memberId;
	private EventType eventType;
	private Date timeStamp;
	private String attribute;
	private String oldValue;
	private String newValue;
	private String eventData;
	private byte isPublished;

	public MemberEvent() {
		//
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

	public byte getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(byte isPublished) {
		this.isPublished = isPublished;
	}
}
