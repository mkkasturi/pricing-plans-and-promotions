package com.wavelabs.resource;

import java.util.Properties;

import org.springframework.stereotype.Component;

import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

@Component
public class KafkaProducer {
	final static String TOPIC = "topicK";
	
    public void produce(String info){
        Properties properties = new Properties();
        properties.put("metadata.broker.list","10.9.9.43:9092");
        properties.put("serializer.class","kafka.serializer.StringEncoder");
        properties.put("request.required.acks", "-1");
        ProducerConfig producerConfig = new ProducerConfig(properties);
        kafka.javaapi.producer.Producer<String,String> producer = new kafka.javaapi.producer.Producer<String, String>(producerConfig);
        KeyedMessage<String, String> message =new KeyedMessage<String, String>(TOPIC,info);
        producer.send(message);
        System.out.println(message);
        producer.close();
    }
}
