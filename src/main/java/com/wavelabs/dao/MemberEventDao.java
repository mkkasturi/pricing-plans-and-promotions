package com.wavelabs.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.wavelabs.model.EventType;
import com.wavelabs.model.MemberEvent;
import com.wavelabs.utility.Helper;

@Component
public class MemberEventDao {
	public void persistEvent(int memberId, EventType eventType, Date timeStamp, String attribute, String oldValue, String newValue, String eventData) {
		Session eventSession = Helper.getSession();
		MemberEvent event = new MemberEvent();
		event.setMemberId(memberId);
		event.setEventType(eventType);
		event.setTimeStamp(timeStamp);
		event.setAttribute(attribute);
		event.setOldValue(oldValue);
		event.setNewValue(newValue);
		event.setEventData(eventData);
		eventSession.save(event);
		Transaction transaction = eventSession.beginTransaction();
		transaction.commit();
	}

	@SuppressWarnings("unchecked")
	public List<MemberEvent> getUnpublishedMemberEvents() {
		Session eventSession = Helper.getSession();
		Criteria criteria = eventSession.createCriteria(MemberEvent.class);
		Criterion criterion = Restrictions.eq("isPublished", (byte) 0);
		criteria.add(criterion);
		List<MemberEvent> events = criteria.list();
		for(MemberEvent event : events){
			System.out.println(event.getId());
		}
		return criteria.list();
	}

	public void changeToPublishedEvent(int eventId) {
		Session eventSession = Helper.getSession();
		MemberEvent event = eventSession.get(MemberEvent.class, eventId);
		event.setIsPublished((byte) 1);
		eventSession.saveOrUpdate(event);
		Transaction transaction = eventSession.beginTransaction();
		transaction.commit();
	}
}
