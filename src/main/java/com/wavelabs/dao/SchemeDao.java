package com.wavelabs.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.wavelabs.model.Scheme;
import com.wavelabs.utility.Helper;

@Component
public class SchemeDao {
	public Scheme persistScheme(Scheme scheme){
		Session session = Helper.getSession();
		session.save(scheme);
		session.beginTransaction().commit();
		session.close();
		return scheme;
	}
	
	public Scheme getScheme(int schemeId){
		Session session = Helper.getSession();
		Scheme scheme = session.get(Scheme.class, schemeId);
		session.close();
		return scheme;
	}
}
