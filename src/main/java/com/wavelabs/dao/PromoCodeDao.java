package com.wavelabs.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.wavelabs.model.PromoCode;
import com.wavelabs.utility.Helper;

@Component
public class PromoCodeDao {
	public PromoCode persistPromoCode(PromoCode promoCode) {
		Session session = Helper.getSession();
		session.save(promoCode);
		session.beginTransaction().commit();
		session.close();
		return promoCode;
	}

	public PromoCode getPromoCode(int promoCodeId) {
		Session session = Helper.getSession();
		PromoCode promoCode =  session.get(PromoCode.class, promoCodeId);
		session.close();
		return promoCode;
	}
	
	public PromoCode updatePromoCode(PromoCode promoCode) {
		Session session = Helper.getSession();
		session.saveOrUpdate(promoCode);
		session.beginTransaction().commit();
		session.close();
		return promoCode;
	}

	public void deletePromoCode(int promoCodeId) {
		Session session = Helper.getSession();
		PromoCode promoCode = session.get(PromoCode.class, promoCodeId);
		session.delete(promoCode);
		session.beginTransaction().commit();
		session.close();
	}
}
