package com.wavelabs.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.dao.MemberEventDao;
import com.wavelabs.model.EventType;
import com.wavelabs.model.MemberEvent;

@Component
public class MemberEventService {
	@Autowired
	MemberEventDao eventDao;
	
	public void pesrsistEvent(int memberId, EventType eventType, Date timeStamp, String attribute, String oldValue, String newValue, String eventData){
		eventDao.persistEvent(memberId, eventType, timeStamp, attribute, oldValue, newValue, eventData);
	}
	
	public List<MemberEvent> getEvents(){
		return eventDao.getUnpublishedMemberEvents();
	}
	
	public void changeToPublishedEvent(int eventId){
		eventDao.changeToPublishedEvent(eventId);
	}
	
}
