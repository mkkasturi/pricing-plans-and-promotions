package com.wavelabs.service;

import java.io.IOException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wavelabs.model.Member;
import com.wavelabs.model.MemberEvent;
import com.wavelabs.resource.KafkaProducer;
import com.wavelabs.utility.Helper;

@Component
public class ScheduledTasks {

	@Autowired
	MemberEventService eventService;

	@Autowired
	KafkaProducer producer;

	@Scheduled(fixedRate = 15000)
	public void reportCurrentTime() throws IOException {
		Session session = Helper.getSession();
		List<MemberEvent> events = eventService.getEvents();
		for (MemberEvent event : events) {
			Member member = session.get(Member.class, event.getMemberId());
			long phoneNumber = member.getPhoneNumber();
			producer.produce(phoneNumber+"- \nHi, "+member.getName()+". YOU " + event.getEventType());
			if (event.getIsPublished() == 0) {
				eventService.changeToPublishedEvent(event.getId());
			}
		}
	}
}
