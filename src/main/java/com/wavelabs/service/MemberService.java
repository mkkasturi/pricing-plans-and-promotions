package com.wavelabs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.dao.MemberDao;
import com.wavelabs.model.Member;

@Component
public class MemberService {
	
	@Autowired
	MemberDao memberDao;

	public Member addMember(Member member) {
		return memberDao.persistMember(member);
	}

	public Member getMember(int memberId) {
		return memberDao.getMember(memberId);
	}

	public Member deleteMember(int memberId) {
		return memberDao.deleteMember(memberId);
	}
	
	public Member updateMember(Member member) {
		return memberDao.updateMember(member);
	}
}
