FROM java:8
ADD target/PricingPlansAndPromotions.jar PricingPlansAndPromotions.jar
ENTRYPOINT ["java","-jar","PricingPlansAndPromotions.jar"]
